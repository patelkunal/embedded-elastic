package org.codearth.searchables.embbededelastic.esnode;

import pl.allegro.tech.embeddedelasticsearch.EmbeddedElastic;
import pl.allegro.tech.embeddedelasticsearch.PopularProperties;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Created by kunal_patel on 11/03/18.
 */
public class ElasticserachNodeServer {

    private static final String CLUSTER_NAME = "es_test";
    private final File DATA_DIR;
    private final EmbeddedElastic embeddedElastic;

    public ElasticserachNodeServer() throws IOException {
        DATA_DIR = Files.createTempDirectory("es_test_data_").toFile();
        embeddedElastic = EmbeddedElastic.builder()
                .withElasticVersion("6.2.2")
                .withSetting(PopularProperties.TRANSPORT_TCP_PORT, 9350)
                .withSetting(PopularProperties.CLUSTER_NAME, CLUSTER_NAME)
                // .withPlugin("analysis-stempel")
                // .withIndex("cars", IndexSettings.builder()
                //         .withType("car", getSystemResourceAsStream("car-mapping.json"))
                //         .build())
                // .withIndex("books", IndexSettings.builder()
                //         .withType(PAPER_BOOK_INDEX_TYPE, getSystemResourceAsStream("paper-book-mapping.json"))
                //         .withType("audio_book", getSystemResourceAsStream("audio-book-mapping.json"))
                //         .withSettings(getSystemResourceAsStream("elastic-settings.json"))
                //         .build())
                .build();
    }

    public void start() throws IOException, InterruptedException {
        embeddedElastic.start();
    }

    public void stop() throws IOException {
        Files.deleteIfExists(DATA_DIR.toPath());
        embeddedElastic.stop();
    }

}
