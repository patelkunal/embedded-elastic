package org.codearth.searchables.embbededelastic.esnode;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by kunal_patel on 11/03/18.
 */
public class AppTests {

    private ElasticserachNodeServer elasticserachNodeServer;
    private static final Logger LOGGER = LoggerFactory.getLogger(AppTests.class);

    @Before
    public void setup() throws IOException, InterruptedException {
        elasticserachNodeServer = new ElasticserachNodeServer();
        elasticserachNodeServer.start();
    }

    @After
    public void teardown() throws IOException {
        elasticserachNodeServer.stop();
    }

    @Test
    public void test() {
        LOGGER.info("TESTING ElasticsearchNodeServer !!");
    }


}
